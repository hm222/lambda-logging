use lambda_http::{run, http::{StatusCode, Response}, service_fn, Error as LambdaError, IntoResponse, Request, RequestPayloadExt};
use log::{error, info, warn, LevelFilter};
use simple_logger::SimpleLogger;
use serde::{Deserialize, Serialize};
use serde_json::json;

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    SimpleLogger::new()
        .with_utc_timestamps()
        .with_level(LevelFilter::Info)
        .init()
        .unwrap();

    info!("Function starting");
    run(service_fn(function_handler)).await?;

    Ok(())
}

pub async fn function_handler(event: Request) -> Result<impl IntoResponse, LambdaError> {
    info!("Processing event: {:?}", event);

    match event.payload::<MyPayload>() {
        Ok(Some(body)) => {
            let name = body.name;
            let response = Response::builder()
                .status(StatusCode::OK)
                .header("Content-Type", "application/json")
                .body(json!({ "message": format!("Hello, {}", name) }).to_string())
                .expect("Failed to render response"); // Consider handling this more gracefully

            Ok(response)
        },
        Ok(None) => {
            warn!("No body found in the request");
            let response = Response::builder()
                .status(StatusCode::BAD_REQUEST)
                .header("Content-Type", "application/json")
                .body(json!({ "error": "Missing body" }).to_string())
                .expect("Failed to render response");

            Ok(response)
        },
        Err(e) => {
            error!("Error processing payload: {:?}", e);
            Err(e.into())
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct MyPayload {
    pub name: String
}
